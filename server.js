// file reading - index.html
const fs = require('fs');
// fastify
const fastify = require('fastify')({
  logger: true
})
// fastify plugin for accepting normal html form posts
// fastify can only support json posts by default
fastify.register(require('fastify-formbody'))

// mysql
var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "password"
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected to mysqld");
  con.query("CREATE DATABASE if not exists contact", function (err, result) {
    if (err) throw err;
    console.log("Database created if not exists");
  });
  con.query("use contact", function (err, result) {
    if (err) throw err;
    console.log("Database contact selected");
  });
  var sql = "CREATE TABLE if not exists contacts ( id int not null auto_increment primary key, fname VARCHAR(255),lname VARCHAR(255), zip VARCHAR(255), address VARCHAR(255))";
  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log("Table contacts created if not exists");
  });

});

// function adds entry to contact db
// entry is 4 item list ['test' , 'test', 'test', 'test']
function add_mysql_entry(entry){
  con.query("use contact", function (err, result) {
    if (err) throw err;
    console.log("Database contact selected");
  });
var sql = `
            INSERT INTO contacts
            (
                fname, lname, zip, address
            )
            VALUES
            (
                ?, ?, ?, ?
            )`;
con.query(sql, entry, function (err, data) {
    if (err) throw err;
    console.log("table test entry completed successfully");
});
}
//add_mysql_entry(['test','test','test','test'])



// routes
// get /
fastify.get('/', function (request, reply) {
	reply.type('text/html').send(fs.readFileSync('index.html', 'utf8'));
})
// get table clear button
fastify.get('/tableClear', function (request, reply) {
  con.query("truncate table contacts;", function (err, result, fields) {
	if (err) throw err;
	console.log('truncating table');
  });
})
// get db
fastify.get('/output', function (request, reply) {
  con.query("select * from contacts order by id desc;", function (err, result, fields) {
	if (err) throw err;
	//console.log(result);
	console.log('reading');
	var ret = "ID - Firstname - Lastname - Zip - Address <br> ";
	result.forEach(v =>{
		//console.log(v)
		//console.log(v['id'] + " " + v['fname'] + " " + v['lname'] + " " + v['zip'] + " " + v['address'] + "<br>");
		ret = ret + v['id'] + " - " + v['fname'] + " " + v['lname'] + " " + v['zip'] + " " + v['address'] + "<br>";
	});
	//reply.type('text/html').send(result);
	//reply.type('text/html').send('lol\n');
	reply.type('text/html').send(ret);
  });

})

// post form
fastify.post("/form", (request, reply) => {
	console.log(request.body)		
	var fname = request.body['fname'];
	var lname = request.body['lname'];
	var zip = request.body['zip'];
	var address = request.body['address'];
	if (( typeof fname == 'undefined') || ( typeof lname == 'undefined' ) || ( typeof zip == 'undefined') || ( typeof address == 'undefined' )) {
		console.log('bad form');
		reply
		    .code(400)
		    .header('Content-Type', 'text/html; charset=utf-8')
		    .send('bad form data')
	}
	// basic input sanitization
	fname = fname.replace(/\W/g, '')
	lname = lname.replace(/\W/g, '')
	zip = zip.replace(/\W/g, '')
	address = address.replace(/\W/g, '')

	console.log(fname + " " + lname + " " + zip + " " + address)
	add_mysql_entry([fname, lname, zip, address])




	reply.redirect('/');

})


// Run the server
fastify.listen(4444, '0.0.0.0', function (err, address) {
  if (err) {
    fastify.log.error(err)
    process.exit(1)
  }
  fastify.log.info(`server listening on ${address}`)
})
